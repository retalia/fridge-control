#include <blink.h>
#include <buttons.h>

enum TempStatus { TOO_HIGH, TOO_LOW, WITHIN_LIMITS }; // if the temperature is too high, we should turn the fridge on, if it's too low, we should turn it off. Otherwise do nothing

// forward declarations
void buttonHandler();
TempStatus tempStatus();

bool on = false; // start in Off status. This is assumed by loop()
bool firstTime = true; // flag used to initialize the fridge when loop() runs for the first time

// read the temperature every measurement every 'measureInterval' seconds and put it in 'temps' array
// keep last 10 readings
#define TEMPS_COUNT 10
float temps[TEMPS_COUNT] = {-100,-100,-100,-100,-100,-100,-100,-100,-100,-100};
unsigned char tempIndex = 0; // points to the first empty temperature position in temps[]
int measureInterval = 1; // keep a reading every _measureInterval_ seconds
unsigned long lastReadingTime = 0; // time we took last measurements as returned by millis()
float normalTemp = -100.0; // average temperature of last 30 seconds. -100 means that it's not set yet

// temparature levels
#define TEMP_A 8.0
#define TEMP_B 6.0
#define TEMP_C 4.0
// least difference from a temperature level that will trigger start/stop. So, if userTemp is
// 7 and the window is 1 then if the temperature rised over 7+1 the fridge will start working.
// if It drops below 7-1, the fridge will stop working.
float triggerWindow = 1.0;
float userTemp; // temperature level specified by the user 
// temperature correction
#define FRIDGE_ON_CORRECTION -1 // the relay is ON the temperature reading is +1. 

// LEDS & BUTTONS
#define ledPort 3
#define buttonPort 4
#define FREEZE_OFF 0
#define FREEZE_A 1
#define FREEZE_B 2
#define FREEZE_C 3
#define FREEZE_ALWAYS 4
unsigned int buttonState = FREEZE_OFF; // OFF, FREEZE_LOW, FREEZE_MED, FREEZE_HIGH, FREEZE_ALWAYS
BlinkInfo blinkingLed(ledPort, 0, 1000, 0, 0);
Button button1(buttonPort, BUTTON_PRESS, buttonHandler);
// RELAY
#define relayPort 2

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  // relay
  pinMode(relayPort, OUTPUT);
  digitalWrite(relayPort, HIGH); // HIGH: turn off, LOW: turn on
  // leds
  blinkingLed.init();
  //buttons
  button1.init();
  userTemp = buttonStateToUserTemp(buttonState);
}

void loop() {
  int sensorVal = analogRead(A0);
  float lastTemp;
  
  // take a measurement every 'measureInterval' seconds
  unsigned long readingTime = millis();
  if ( (readingTime - lastReadingTime)/1000 > measureInterval ) { 
    // time to measure and store in temps[]
    lastTemp = getTemperature();
    temps[tempIndex] = lastTemp;
    // recycle tempIndex
    tempIndex ++;
    if (tempIndex == TEMPS_COUNT)
      tempIndex = 0;
    // setup next reading time
    lastReadingTime = readingTime; 
    // update normalTemp global
    normalizeTemp();

    Serial.print("normalized temp: "); Serial.print(normalTemp); Serial.print(" last temp: "); Serial.println(lastTemp);
    dumpTemps();

    // operate fridge based on buttonState, temps and status
    printTempStatus();
    if (buttonState == FREEZE_ALWAYS) { 
      if (!on) turnOn();
    } else
    if (buttonState == FREEZE_OFF) { 
      if (on) turnOff();
    } else
    // button state is neither FREEZE_ALWAYS nor FREEZE_OFF
    if (tempStatus() == WITHIN_LIMITS) {
      // do nothing. If it's already On, leave it that way. If it's Off, so be it.
    } else {
      if (tempStatus() == TOO_HIGH && !on)
        turnOn();
      else
      if (tempStatus() == TOO_LOW && on)
        turnOff();
    }

    Serial.print("fridge on: "); Serial.println(on);
  }

  blinkingLed.update();
  button1.update();
  
  delay(100);
}

void printTempStatus() { 
  Serial.print("tempStatus: ");
  if (tempStatus() == TOO_HIGH)
    Serial.println("TOO_HIGH");
  else
  if (tempStatus() == TOO_LOW)
    Serial.println("TOO_LOW");
  else
  if (tempStatus() == WITHIN_LIMITS)
    Serial.println("WITHIN_LIMITS");
  else
    Serial.println("UNKNOWN temp status");
}

TempStatus tempStatus() { 
    if (normalTemp > userTemp + triggerWindow) {
      //oops, temperature has risen too much
      return TOO_HIGH;
    }
    if (normalTemp < userTemp - triggerWindow) { 
     // oops, temparature has dropped too low
      return TOO_LOW;
    }  
    return WITHIN_LIMITS;
}

void turnOn() {
  if (!on) {
    Serial.print("turned on\n");
    on = true;
    digitalWrite(relayPort, LOW);
  }
}

void turnOff() {
  if (on) {
    Serial.println("turned off");
    on = false;
    digitalWrite(relayPort, HIGH);
  }
}

// update global vars according to buttonState
void buttonHandler() { 
  // button states are changed incrementally FREEZE_A(0) -> FREEZE_MED ... -> FREEZE_ALWAYS  
  if (buttonState == FREEZE_ALWAYS)
    buttonState = FREEZE_OFF;
  else
    buttonState ++;
  userTemp = buttonStateToUserTemp(buttonState);

  Serial.print("buttonState: "); Serial.println(buttonState);

  if (buttonState == FREEZE_A ) { 
    blinkingLed.reprogram(300, 300, buttonState, 1000);
  } else 
  if (buttonState == FREEZE_B ) { 
    blinkingLed.reprogram(300, 300, buttonState, 1000);
  } else
  if (buttonState == FREEZE_C ) { 
    blinkingLed.reprogram(300, 300, buttonState, 1000);
  } else
  if (buttonState == FREEZE_ALWAYS) { 
    blinkingLed.reprogram(1000, 0, 3, 0);
  } else
  if (buttonState == FREEZE_OFF) { 
    blinkingLed.reprogram(0,1000,0,0);
  }
}


float buttonStateToUserTemp(unsigned int button_state) { 
  if (button_state == FREEZE_A) return TEMP_A;
  if (button_state == FREEZE_B) return TEMP_B;
  if (button_state == FREEZE_C) return TEMP_C;
}


// get the average of all temparature readings (all reading stored in array) and store in 'normalTemp' global
void normalizeTemp() { 
  float sum;
  int summedCount;
  sum = 0;
  summedCount = 0;
  for ( char i=0; i<TEMPS_COUNT;i++) { 
    if (temps[i] != -100) { 
      sum += temps[i];
      summedCount ++;
    }
  }
  if (summedCount > 0)
    normalTemp = sum/summedCount;
  else
    normalTemp = -100;
}

// read the temparature from the sensor and convert to Celcius
float getTemperature() { 
  int sensorVal;
  sensorVal = analogRead(A0);
  // apply temparature correction. See - https://gitlab.com/otsakir/fridge-control/issues/3
  if (on) {
    sensorVal += FRIDGE_ON_CORRECTION;
  }
  Serial.print("sensorVal: "); Serial.println(sensorVal);
  return convertToCelcius(sensorVal);
}

float convertToCelcius(int sensorVal) { 
  float voltage = (sensorVal/1024.0) *5.0;
  float temperature = (voltage - 0.5) * 100.0;

  return temperature;
}

void dumpTemps() { 
  Serial.print("temps: [");
  for (int i=0; i<TEMPS_COUNT; i++) {
    Serial.print(temps[i]);  Serial.print(",");
  }
  Serial.println("]");
}

float updateTemperature() { 

}
