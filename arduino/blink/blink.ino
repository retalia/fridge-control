#include "blink.h"

BlinkInfo blink1(3, 500, 300, 0, 0); //The LED attached to port 4 stays 0.5 sec on and 0.3 sec off
//LED blink three times and then wait 1 sec
//BlinkInfo blink1(3, 500, 300, 3, 1000); // LED blink three times and then wait 1 sec
//BlinkInfo blink1(3, 1000, 0, 0, 0); // always on
//BlinkInfo blink1(3, 0, 1000, 0, 0); // always off
//BlinkInfo blink2(4,300,300,3, 1000); 

void setup() {
  Serial.begin(9600);
  blink1.init();
  //blink2.init();
}

int i = 0;

void loop() {
  blink1.update();
  //blink2.update();
  delay(100);
  i ++;
  if (i == 100) { 
    blink1.reprogram(1000,1000,0,1000);    
  }
}


