#ifndef _BLINK_H_
#define _BLINK_H_

/**

*** LED blinking library

BlinkInfo blink1(port, msOn, msOff, loopCount, loopInterval);
blink1.reprogram(msOn, msOff, loopCount, loopInterval);

*** Examples

* Simple LED blinking on and off:

  BlinkInfo blink1(4, 500, 300, 0, 0);

  The LED attached to port 4 stays 0.5 sec on and 0.3 sec off

* LED blink three times and then wait 1 sec:

  BlinkInfo blink1(4, 500, 300, 3, 1000);

* LED always on

  BlinkInfo blink1(4, 1000, 0, 0, 0);

* LED always off

  BlinkInfo blink1(4, 0, 1000, 0, 0);

* Change existing LED configuration. From always off to always on

  BlinkInfo blink1(4, 0, 1000, 0, 0);
  // ...
  blink1.reprogram(1000,0,0,0);

*/
struct BlinkInfo {
  bool on;
  bool looping;
  unsigned long lastSwitchTime;
  unsigned int msOn;
  unsigned int msOff;
  unsigned char port;
  unsigned char loops; // use this if  > 0. Otherwise ignore it
  unsigned int loopInterval; // how much time to wait in 'off' state after loop is done
  unsigned char loopCount; // starts from 0. The count signifies the number of loop completed so far.

  BlinkInfo(unsigned char pport, unsigned int pmsOn, unsigned int pmsOff, unsigned char ploops, unsigned int ploopInterval) {
    msOn = pmsOn;
    msOff = pmsOff;
    port = pport;
    on = false;
    loopCount = 0;
    looping = false;
    loops = ploops;
    loopInterval = ploopInterval;
  }

  // typically called in setup()
  void init() {
    pinMode(port, OUTPUT);
    digitalWrite(port, on);
    lastSwitchTime = millis();

    dump();
  }


  void loopTime() {
    loopCount = 0; // start from the beggining
    looping = true;
    //Serial.println("start loop");
  }

  // define what happens when we turn on the led
  void turnOn(unsigned long currentMillis) {
      on = true;
      lastSwitchTime = currentMillis;
      digitalWrite(port, on);
  }

  // define what happens when we turn off the led
  void turnOff(unsigned long currentMillis) {
      on = false;
      lastSwitchTime = currentMillis;
      digitalWrite(port, on);
      if (loops >= 1) {
          if (loopCount == loops-1) {
            loopTime();
          } else {
            loopCount ++;
          }

      }
  }

  // call this regularly in loop() to keep led state in updated
  void update() {
    unsigned long currentMillis = millis();
    unsigned long elapsed = currentMillis - lastSwitchTime;
    // always off
    if (msOff > 0 && msOn == 0) {
      if (on) turnOff(currentMillis);
      return;
    }

    if (looping) {
      if (elapsed > loopInterval) {
        // done looping. Start again
        looping = false;
        turnOn(currentMillis);
      }
    } else {
      if (on && msOff > 0) {
        if (elapsed > msOn) {
          turnOff(currentMillis);
        }
      } else { // if off
        if (elapsed > msOff) {
          turnOn(currentMillis);
        }
      }
    }
  }

  void reprogram(unsigned int pmsOn, unsigned int pmsOff, unsigned char ploops, unsigned int ploopInterval) {
    unsigned long currentMillis = millis();
    turnOff(currentMillis);
    lastSwitchTime = currentMillis;
    looping = false;
    msOn = pmsOn;
    msOff = pmsOff;
    loops = ploops;
    loopInterval = ploopInterval;
    loopCount = 0;
    dump();
  }

  void dump() {
    Serial.print("msOn: "); Serial.print(msOn);
    Serial.print("msOff: "); Serial.println(msOff);
  }

};

















#endif

