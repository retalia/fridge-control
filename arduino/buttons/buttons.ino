#include "button.h"

void button1Handler(ButtonEvent event, Button& button) {
  Serial.println("button1 pressed");
}

Button button1(4, BUTTON_PRESS, button1Handler);

void setup() {
  Serial.begin(9600);
  button1.init();
}

void loop() {
  // put your main code here, to run repeatedly:
  button1.update();
  int value = digitalRead(4);
  Serial.print("read value ");
  Serial.println(value);

  delay(300);
}
